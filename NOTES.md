## Test

Tasks:
- API Key based validation
- Username/Password based validation
- CSRK based validation

Links:
- https://fastapi.tiangolo.com/tutorial/security/
- https://www.pedaldrivenprogramming.com/2020/12/adding-real-user-authentication-to-fastapi/
- https://www.stackhawk.com/blog/csrf-protection-in-fastapi/
- https://www.youtube.com/watch?v=eWEgUcHPle0