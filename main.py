from operator import le
from fastapi import FastAPI, Response, status, Path, Body
from pydantic import BaseModel, Field
from typing import Union
import mysql.connector
import cryptography


app = FastAPI()

user='root'
password='password'
db_name='mydb'
host = '172.19.0.2'
connection = mysql.connector.connect(host=host,user=user,port=3306,password=password,database=db_name)

class create_dict(dict):
    def __init__(self):
        self = dict()
    def add(self, key, value):
        self[key] = value

class Item(BaseModel):
    item_id: int
    ranks: Union[str, None] = None
    description: Union[str, None] = None

@app.get("/")
def root():
    return "This is a script to connect Python and Database using MYSQL Connector and FastAPI"

@app.get("/fastdb/view")
def view():   
    data = { "message": "db connected" }
    return data

@app.get("/fastdb/showdb")
def showDB():
    cursor = connection.cursor()
    db_name = []
    cursor.execute("Show databases")
    for db in cursor:
        db_name.append(db)
    print(cursor.rowcount, "record(s) affected")
    cursor.close()
    return db_name
               
@app.get("/fastdb/showtables")
def showTables():                                                   
    cursor=connection.cursor()
    tb_name = []
    cursor.execute("Show tables")                                 
    for tb in cursor:                                             
        tb_name.append(tb)                                                
    print(cursor.rowcount, "record(s) affected")
    cursor.close()
    return tb_name

@app.get("/fastdb/showtables/{tb_name}")    
def showRows_of_rank(tb_name: str, response:Response):                                             
    cursor=connection.cursor()                                    
    sqlInput=f"SELECT * FROM {tb_name}"                               
    cursor.execute(sqlInput)                                      
    result=cursor.fetchall()
    if result is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": f"There is no data on {tb_name}", "status": response.status_code}                                     
    
    response = {"message": f"Succes to get the data from {tb_name}", "data": result}
    cursor.close()  
    return response

@app.delete("/fastdb/delete_rank")
def deleteRow_in_rank(item: Item, response:Response):                                  
    cursor=connection.cursor()
    sqlInput=f"DELETE FROM tb_rank where id=%s"
    data = (item.item_id,)                                                             
    cursor.execute(sqlInput,data)
    connection.commit()                                                                             
    response = {"message": f"Data deleted from table rank with id: {item.item_id}"}
    cursor.close()
    return response                                           
       
@app.post("/fastdb/insert_rank")
def insert_into_rank(item: Item):                                     
    cursor=connection.cursor()
    sqlInput="INSERT into tb_rank (id,ranks,description) values (%s,%s,%s)"   
    data=(item.item_id, item.ranks, item.description)                                               
    cursor.execute(sqlInput,data)
    connection.commit()
    cursor.close()
    return {"message": "1 row inserted"}
    
@app.put("/fastdb/update_rank")
def updateRank(item: Item):                                                   
    cursor = connection.cursor()              
    sqlInput = f"UPDATE tb_rank SET ranks = %s, description =%s WHERE id = %s"
    data=(item.ranks, item.description, item.item_id)
    cursor.execute(sqlInput, data)                                      
    connection.commit()                                             
    print(cursor.rowcount, "record(s) affected")
    cursor.close()
    return {"message": "1 row updated"}
                                                                                                   