# syntax=docker/dockerfile:1

FROM python:3.8.13-slim-bullseye 

WORKDIR /datavizz/fastsql

COPY ./requirements.txt /datavizz/fastsql/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /datavizz/fastsql/requirements.txt

COPY . /datavizz/fastsql

CMD ["python","-m","uvicorn", "main:app", "--host", "0.0.0.0" , "--port", "3000", "--reload"]
